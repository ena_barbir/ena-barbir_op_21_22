#include <iostream>
using namespace std;
void min_max(int arr[], int len, int*min, int*max)
{
    if (len == 0)
        return;
    if (*min > arr[len])
        *min = arr[len];
    else if (*max < arr[len])
        *max = arr[len];

    return min_max(arr, (len - 1), min, max);
}
int main() {
    int arr[] = { 3,8,18,26,53,4,7,14 };
    int len = sizeof(arr) / sizeof(arr[0]) - 1;
    int najmanji = arr[0];
    int najveci = arr[0];
    min_max(arr, len, &najmanji, &najveci);
    cout << "najmanji element niza je: " << najmanji << endl;
    cout << "najveci element niza je: " << najveci << endl;
    return 0;
}

