/*
#include <iostream>
#include <time.h>
using namespace std;

struct matrica {
	int x, y;
	float**m;
};

void generiraj(int a, int b,matrica*m) {
	m->x = a;
	m->y = b;
	m->m = new float* [8];
	for (int i = 0; i < m->x; i++) {
		m->m[i] = new float[m->y];
	}
}
void unos(matrica* matrica) {
	for (int i = 0; i < matrica->x; i++) {
		for (int j = 0; j < matrica->y; j++) {
			cout << "Unesite element" << i << " " << j << endl;
			cin >> matrica->m[i][j];
		}
	}
}
void random(matrica* matrica,int a,int b) {
	for (int i = 0; i < matrica->x; i++) {
		for (int j = 0; j < matrica->y; j++) {
			matrica->m[i][j]=a+rand()%(b-a+1);
		}
	}
}
matrica zbroj(const matrica* m1,const matrica* m2) {
	matrica zbroj{};
	if ((m1->x != m2->x) || (m1->y != m2->y)) {
		zbroj.x = 0;
		zbroj.y = 0;
		zbroj.m = NULL;
		return zbroj;
	}
	else {
		generiraj(m1->x, m1->y, &zbroj);
		for (int i = 0; i < m1->x; i++) {
			for (int j = 0; j < m1->y; j++) {
				zbroj.m[i][j] = m1->m[i][j]+m2->m[i][j];
			}
		}
	}
	return zbroj;
}
matrica oduzmi(const matrica* m1, const matrica* m2) {
	matrica razlika{};
	if ((m1->x != m2->x) || (m1->y != m2->y)) {
		razlika.x = 0;
		razlika.y = 0;
		razlika.m = NULL;
		return razlika;
	}
	else {
		generiraj(m1->x, m1->y, &razlika);
		for (int i = 0; i < m1->x; i++) {
			for (int j = 0; j < m1->y; j++) {
				razlika.m[i][j] = m1->m[i][j] - m2->m[i][j];
			}
		}
	}
	return razlika;
}
matrica umnozak(const matrica* m1, const matrica* m2) {
	matrica umnozak{};
	if ((m1->y != m2->x)) {
		umnozak.x = 0;
		umnozak.y = 0;
		umnozak.m = NULL;
		return umnozak;
	}
	else {
		generiraj(m1->x, m2->y, &umnozak);
		for (int i = 0; i < umnozak.x; i++)
		{
			for (int j = 0; j < umnozak.y; j++)
			{
				int sum = 0;
				for (int k = 0; k < m1->y; k++)
				{
					sum += (m1->m[i][k] * m2->m[k][j]);
				}
				umnozak.m[i][j] = sum;
			}
		}

	}
	return umnozak;

}
matrica transp(matrica* m1)
{
	matrica temp;
	generiraj(m1->y, m1->x, &temp);

	for (int i = 0; i < m1->x; i++) {
		for (int j = 0; j < m1->y; j++) {
			temp.m[j][i] = m1->m[i][j];
		}
	}
	return temp;
}
void unisti(matrica* m) {
	for (int i = 0; i < m->x; i++) {
		delete[]m->m[i];
	}
	delete m->m;
}
void ispis(const matrica* m) {
	if (m->m != NULL) {
		for (int i = 0; i < m->x; i++) {
			for (int j = 0; j < m->y; j++) {
				cout << m->m[i][j] << " ";
			}
			cout << endl;
		}
	}
	else {
		cout << "Invalid operation" << endl;
	}
}




int main() {
	srand(time(NULL));
	matrica m1;
	generiraj(3,5,&m1);
	random(&m1, 5, 15);
	matrica m2;
	generiraj(5, 3, &m2);
	random(&m2, 5, 15);
	ispis(&m1);
	cout << "----------------" << endl;
	ispis(&m2);
	cout << "----------------" << endl;
	matrica zbroji = transp(&m1);
	ispis(&zbroji);

}

*/