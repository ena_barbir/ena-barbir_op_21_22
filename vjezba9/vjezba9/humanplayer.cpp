#include "humanplayer.hpp"

int HumanPlayer::move() {
	int num = 1000;
	int sum = 0;
	do {
		cout << "Unesite koliko kovanica zelite odabrati" << endl;
		cin >> num;
	} while (num>3 || num<0);
	if (num == 3) {
		return 8;
	}
	else {
		for (int i = 0; i < num; i++) {
			int x = 500;
			while (true) {
				cout << "Unesite kovanicu" << endl;
				cin >> x;
				if ((x == 5 || x == 2 || x == 1) && x!=sum) {
					break;
				}
			}
			cout << "Kovanica odabrana !!" << endl;
			sum += x;
		}
	}
	return sum;
}
int HumanPlayer::predict() {
	int pred = 1000;
	do {
		cout << "Unesite vase predvidanje" << endl;
		cin >> pred;
	} while (pred <= 0 || pred > 8);
	return pred;
}

void HumanPlayer::increment_points()
{
	this->points++;
}

int HumanPlayer::get_points()
{
	return this->points;
}

