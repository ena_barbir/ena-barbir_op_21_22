#include <iostream>
#include "humanplayer.hpp"
#include "computerplayer.hpp"
#include "player.hpp"
#include "game.hpp"

int main() {
	Player* p1 = new HumanPlayer;
	Player* p2 = new ComputerPlayer;
	Game g(p1, p2);
	g.start();
	return 0;
}