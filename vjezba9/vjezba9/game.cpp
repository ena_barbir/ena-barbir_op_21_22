#include "game.hpp"
Game::Game(Player*p1,Player*p2) {
	this->p1 = p1;
	this->p2 = p2;
}
void Game::start() {
	while (1) {
		cout << "Odigrajte potez i predvidanje" << endl;
		int movep1 = p1->move();
		int movep2 = p2->move();
		int predp1 = p1->predict();
		int predp2 = p2->predict();
		this->check(movep1, movep2, predp1, predp2);
		cout << "Igrac 1 odigrao je " << movep1 << endl;
		cout << "Igrac 2 odigrao je " << movep2 << endl;
		cout << "Igrac 1 ima " << p1->get_points()<<" bodova" << endl;
		cout << "Igrac 2 ima " << p2->get_points() << " bodova" << endl;
		if (check_game_over() == 0) {
			cout << "Igra gotova,nerjeseno" << endl;
			return;
		}
		if (check_game_over() == 1) {
		cout << "Igra gotova,pobjednik je igrac 1" << endl;
		return;
		}
		if (check_game_over() == 2) {
			cout << "Igra gotova,pobjednik je igrac 2" << endl;
			return;
		}
	}
}
void Game::check(int movep1, int movep2, int predp1, int predp2) {
	if (movep1 == predp2) {
		p2->increment_points();
	}
	if (movep2 == predp1) {
		p1->increment_points();
	}
}
int Game::check_game_over() {
	if (p1->get_points() == goal && p2->get_points() == goal) {
		return 0;
	}
	else if (p1->get_points() == goal) {
		return 1;
	}
	else if (p2->get_points() == goal) {
		return 2;
	}
	else
		return -1;

}