#pragma once
#include "player.hpp"

class ComputerPlayer : public Player {
private:
public:
	int move();
	int predict();
	void increment_points();
	int get_points();
};