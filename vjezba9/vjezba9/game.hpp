#pragma once
#include "player.hpp"
#include "humanplayer.hpp"
#include "computerplayer.hpp"

class Game {
private:
	Player* p1;
	Player* p2;
	int goal = 3;
public:
	Game(Player*p1,Player*p2);
	void start();
	int check_game_over();
	void check(int movep1, int movep2, int predp1, int predp2);
};