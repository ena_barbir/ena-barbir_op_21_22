#pragma once
#include <vector>
#include <iostream>
#include <cstdlib>
using namespace std;

class Player {
protected:
	int points = 0;
//broj bodova
public:
	virtual int move()=0;
	virtual int predict()=0;
	virtual void increment_points() = 0;
	virtual int get_points() = 0;

};