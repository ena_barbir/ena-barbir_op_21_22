#include "computerplayer.hpp"


int ComputerPlayer::move() {
	vector<int> possible = { 1,2,5 };
	int sum = 0;
	int num_of_coins =rand()%possible.size();
	if (num_of_coins == 1) {
		return possible[rand() % 3];
	}
	else if (num_of_coins == 3) {
		return 8;
	}
	else {
		sum += possible[rand() % 3];
		int tmp = sum;
		do {
			tmp= possible[rand() % 3];
		} while (tmp == sum);
		sum += tmp;
	}
	return sum;
}
int ComputerPlayer::predict() {
	return rand() % 8;
}

void ComputerPlayer::increment_points()
{
	this->points++;
}

int ComputerPlayer::get_points()
{
	return this->points;
}
