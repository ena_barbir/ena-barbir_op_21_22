#include "hangman.hpp"

int main() {
	Hangmanmodel model;
	HangmanDisplay display(model);
	Hangmancontroller controller(model, display);
	controller.play();
}