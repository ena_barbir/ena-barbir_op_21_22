#include <iostream>
#include <cstdlib>
#include <vector>
#include <string.h>
#include <fstream>
#include <sstream>
using namespace std;
class Hangmanmodel {
private:
	vector<string>movie_list;
	string guessMovie;
	int lives;
public:
	Hangmanmodel();
	string getMovie();
	void decrement_lives();
	int getlives();
};
class HangmanDisplay {
private:
	Hangmanmodel model;
public:
	HangmanDisplay(){}
	HangmanDisplay(Hangmanmodel model) {
		this->model = model;
	}
	void displaystatus(string s);
	void displayletters(string s);
	void displaylives(int x);
};
class Hangmancontroller {
private:
	Hangmanmodel model;
	HangmanDisplay display;
	string guessed = "";
public:
	Hangmancontroller(){}
	Hangmancontroller(Hangmanmodel model, HangmanDisplay display) {
		this->model = model;
		this->display = display;
	}
	char entry();
	void turn();
	void play();
	bool gameover();
	bool playerwin();
};
	