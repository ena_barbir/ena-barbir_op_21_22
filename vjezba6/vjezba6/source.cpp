#include "hangman.hpp"

Hangmanmodel::Hangmanmodel() {
	this->lives = 8;
	std::ifstream file("movies.txt");
	string line;
	
	while (getline(file, line)) {
		this->movie_list.push_back(line);
	}
	int rand_movie =rand()%movie_list.size();
	this->guessMovie = movie_list[rand_movie];
}

string Hangmanmodel::getMovie() {
	return this->guessMovie;
}
void Hangmanmodel::decrement_lives() {
	this->lives--;
}
int Hangmanmodel::getlives() {
	return this->lives;
}
void HangmanDisplay::displaystatus(string guessed) {
	string movie = model.getMovie();
	for (int i = 0; i < movie.length(); i++) {
		if (movie[i] == ' ') {
			cout << " ";
		}
		if (guessed.find(movie[i])!= std::string::npos) {
			cout << movie[i] << " ";
		}
		else
			cout << "__ ";
	}
	cout << endl;
}

void HangmanDisplay::displayletters(string s) {
	cout << "You used: " << endl;
	for (int i = 0; i < s.size(); i++) {
		cout << s[i] << " ";
	}
	cout << endl;
}
void HangmanDisplay::displaylives(int x) {
	cout << "You have " << x << " lives" << endl;
}

char Hangmancontroller::entry() {
	char c;
	cout << "Input a letter:" << endl;
	cin >> c;
	return c;
}
void Hangmancontroller::turn() {
	char c = entry();
	string movie = model.getMovie(); 
	// Provjera dali se u movie nalazi uneseno slovo
	if (movie.find(c) != std::string::npos) {
		//Ako se nalazi dodamo ga u string guessed
		guessed.push_back(c);
	}
	else {
		this->model.decrement_lives();
	}
}
bool Hangmancontroller::gameover() {
	if (!this->model.getlives()) {
		return true;
	}
	else {
		return false;
	}
}
bool Hangmancontroller::playerwin() {
	string movie = this->model.getMovie();
	for (int i = 0; i < movie.size(); i++) {
		int flag = 0;
		for (int j = 0; j < guessed.size(); j++) {
			if (movie[i] == guessed[j])
				flag = 1;
		}
		if (!flag)
			return false;
	}
	return true;
}
void Hangmancontroller::play() {
	cout << "Welcome" << endl;
	while (1) {
		this->display.displaystatus(this->guessed);
		this->display.displaylives(this->model.getlives());
		this->turn();
		if (this->gameover()) {
			cout << "You are out of lives" << endl;
			cout << "Game Over!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
			return;
		}
		if (this->playerwin()) {
			cout << "Congratulations,you win!!!" << endl;
			return;
		}
	}
}