#pragma once
#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <fstream>
using namespace std;

vector<int>initial(string s);
int greater_than(int x,const vector<int>&v);
void minimum_maximum(int& min, int& max, const vector<int>& v);
bool delete_condition(int x);
void deletesmaller(vector<int>& v);
bool deletefnc(int x);
void print(const vector<int>& v);
void sort(vector<int>& v);
bool sortfnc(int x, int y);