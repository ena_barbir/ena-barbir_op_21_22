#include "header.hpp"

vector<int>initial(string s) {
	vector<int> temp;
	ifstream f;
	f.open(s);
	if (f.is_open()) {
		istream_iterator<int>start(f);
		istream_iterator<int>end;
		copy(start, end, back_inserter(temp));
	}
	else {
		throw int();
	}
	return temp;
}
bool cmpfnc(int x) {
	if (x > 500) {
		return true;
	}
	else {
		return false;
	}
}
int greater_than(int x,const vector<int>&v)
{
	return count_if(v.begin(), v.end(), cmpfnc);
}
void minimum_maximum(int& min, int& max,const vector<int>&v) {
	max = *(max_element(v.begin(), v.end()));
	min = *(min_element(v.begin(), v.end()));
}
bool delete_condition(int x) {
	if (x < 300) {
		return true;
	}
	else {
		return false;
	}
}
bool deletefnc(int x) {
	if (x < 300) {
		return true;
	}
	return false;
}
void deletesmaller(vector<int>& v)
{
	v.erase(remove_if(v.begin(), v.end(), deletefnc), v.end());
}
bool sortfnc(int x, int y) {
	if (x > y) {
		return true;
	}
	return false;
}
void sort(vector<int>& v) {
	sort(v.begin(), v.end(), sortfnc);
}
void print(const vector<int>& v) {
	copy(v.begin(), v.end(),ostream_iterator<int>(cout, ", "));
}


