#pragma once

#include <iostream>

using namespace std;

void minimaxi(int niz[], int& mini, int& maxi);

int& fun(int(&niz)[6],int x);


struct PRAVOKUTNIK {
    int x1, y1, x2, y2;
};
struct KRUZNICA {
    int radius;
    int x, y;
};
int intersect(PRAVOKUTNIK(&arr)[5], KRUZNICA& krug);

struct vektor {
    int fizicka_v;
    int logicka_v;
    int* niz;
};
void vector_new(vektor& v1);

int vector_size(vektor& v1);

int vector_front(vektor& v1);

void vector_delete(vektor& v1);

void vector_push_back(vektor& v1, int broj);

void vector_pop_back(vektor& v1);

int vector_back(vektor& v1);