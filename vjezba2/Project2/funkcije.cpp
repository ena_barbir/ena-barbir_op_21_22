#pragma once

#include"Header.h"

using namespace std;
/*
//zad 1
void minimaxi(int niz[], int& mini, int& maxi)
{
    mini = niz[0];
    maxi = niz[0];
    for (int i = 0; i < 7; i++)
    {
        if (niz[i] > maxi)
            maxi = niz[i];
        else if (niz[i] < mini)
            mini = niz[i];
    }
}

//zad 2
int& fun(int(&niz)[6],int x) {
    return niz[x];
}
*/

/*
//zad 3

int intersect(PRAVOKUTNIK(&arr)[], KRUZNICA& krug) {
    int counter = 0;
    for (int i = 0; i < 5; i++) {
        int polovistex = (arr[i].x1 + arr[i].x2) / 2;
        int polovistey = (arr[i].y1 + arr[i].y2) / 2; 
        int dijagonala = sqrt((arr[i].x1 - arr[i].x2) * (arr[i].x1 - arr[i].x2) + (arr[i].y1 - arr[i].y2) * (arr[i].y1 - arr[i].y2));
        if (sqrt(krug.x - polovistex) * (krug.x - polovistex) + (krug.y - polovistey) * (krug.y - polovistey) < krug.radius + dijagonala / 2)
            counter++;
    }
    return counter;
}
*/

//zad 4


void vector_new(vektor& v1) {
    v1.niz = new int[v1.fizicka_v];
    v1.logicka_v = 0;
}

/* returns the number of the elements in the vector */
int vector_size(vektor& v1) {
    return v1.logicka_v;
}
/*returns a reference to the first element in the vector*/
int vector_front(vektor& v1) {
    return v1.niz[0];
}
void vector_delete(vektor& v1) {
    delete[] v1.niz;
}
/* adds a new element at the end of the vector */
void vector_push_back(vektor& v1, int broj) {
    v1.niz[v1.logicka_v] = broj;
    if (v1.logicka_v == v1.fizicka_v - 1) {
        int* temp = v1.niz;
        v1.niz = new int[2 * v1.fizicka_v];
        for (int i = 0; i < v1.fizicka_v; i++) {
            v1.niz[i] = temp[i];
        }
        delete[] temp;
    }
    v1.logicka_v++;
}
/* removes the last element in the vector */
void vector_pop_back(vektor& v1) {
    if (v1.logicka_v)
        v1.logicka_v--;
    else
        cout << "Vektor prazan";

}
/*returns a reference to the last element in the vector*/
int vector_back(vektor& v1) {
    if (v1.logicka_v)
        return v1.niz[v1.logicka_v - 1];
    return 0;
}
