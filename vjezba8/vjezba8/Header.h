#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
using namespace std;

class timer {
private:
	int hours;
	int minutes;
	double seconds;
	friend class penalty;
public:
	timer();
	timer(int, int, double);
	timer& operator+=(const timer& t);
	timer& operator/=(int x);
	void round();
	double toseconds();
	friend ostream& operator<<(ostream& o, const timer& t);
	friend bool operator<(timer& t1, timer& t2);
	friend double operator-(timer& t1, timer& t2);
};
class penalty {
private:
	int penal;
public:
	penalty(int x) {
		penal = x;
	}
	void operator()(timer& t) {
		t.seconds += penal;
	}
};

