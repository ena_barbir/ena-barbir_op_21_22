#include "Header.h"
timer::timer() {
	hours = 0;
	minutes = 0;
	seconds = 0;
}
timer::timer(int a, int b, double c) {
	hours = a;
	minutes = b;
	seconds = c;
}
timer& timer::operator+=(const timer& t) {
	hours += t.hours;
	minutes += t.minutes;
	seconds += t.seconds;
	round();
	return *this;
}
void timer::round() {
	while (seconds > 60) {
		minutes++;
		seconds = seconds - 60;
	}
	while (minutes > 60) {
		hours++;
		minutes = minutes - 60;
	}
}
double timer::toseconds() {
	return (double)(this->hours * 3600 + this->minutes * 60 + this->seconds);
}
timer& timer::operator/=(int x) {
	double seconds = this->toseconds();
	seconds = double(seconds / x);
	this->hours = seconds / 3600;
	seconds = seconds - hours * 3600;
	this->minutes = seconds / 60;
	seconds = seconds - minutes * 60;
	this->seconds = seconds;
	this->round();
	return *this;
}
ostream& operator<<(ostream& o, const timer& t) {
	return o <<"hours: "<<t.hours<<" " << "minutes: " << t.minutes << " " << "seconds: " << t.seconds<<endl;
}
bool operator<(timer& t1, timer& t2) {
	if (t1.hours != t2.hours) {
		if (t1.hours < t2.hours) {
			return true;
		}
		else
			return false;
	}
	else if (t1.minutes != t2.minutes) {
		if (t1.minutes < t2.minutes) {
			return true;
		}
		else
			return false;
	}
	else {
		if (t1.seconds < t2.seconds) {
			return true;
		}
		else
			return false;
	}
}
double operator-(timer& t1, timer& t2) {
	double sec1 = t1.toseconds();
	double sec2 = t2.toseconds();
	return abs(sec2 - sec1);
}
