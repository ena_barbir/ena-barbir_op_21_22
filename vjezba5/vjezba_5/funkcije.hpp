#include <iostream>
#include<time.h>
using namespace std;


class tocka {
private:
	double x, y, z;
public:
	void postavi(double a, double b, double c);
	void postavi_sluc(int a, int b);
	void dohvati(double(&niz)[3]);
	double udaljenost2D(tocka& t1);
	double udaljenost3D(tocka& t);
};

class oruzje {
private:
	int stanje;
	int max;
	tocka polozaj;
public:
	void shoot();
	void reload();
	double z_koordinata();
	void set(tocka& t1,int br);
	int getstatus();
};
class meta {
private:
	tocka tocka1;
	tocka tocka2;
	bool shot;
public:
	void get_position(double& dg, double& gg);
	void postavi(tocka& t1, tocka& t2);
	void pogodena(bool b);
};
int pucaj(meta* mete, oruzje& oruzje, int n);
int generate();