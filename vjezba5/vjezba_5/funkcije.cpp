#include "funkcije.hpp"
//zad 1
void tocka::postavi(double a=0, double b=0, double c=0)
{
	x = a;
	y = b;
	z = c;
}

void tocka::postavi_sluc(int a, int b)
{
	x = a + rand() % (b - a + 1);
	y = a + rand() % (b - a + 1);
	z = a + rand() % (b - a + 1);
}

void tocka::dohvati(double(&niz)[3])
{
	niz[0] = x;
	niz[1] = y;
	niz[2]= z;
}

double tocka::udaljenost2D(tocka& t1)
{
	return sqrt((x - t1.x) * (x - t1.x) + (y - t1.y) * (y - t1.y));
}

double tocka::udaljenost3D(tocka& t)
{
	return sqrt((x - t.x) * (x - t.x) + (y - t.y) * (y - t.y) + (z - t.z) * (z - t.z));
}

//zad 2
void oruzje::reload() {
	stanje = max;
}
double oruzje::z_koordinata() {
	double niz[3];
	polozaj.dohvati(niz);
	return niz[2];
}
void oruzje::shoot() {
	if (stanje)
		stanje--;
	else
		cout << "Spremnik prazan" << endl;
}
void oruzje::set(tocka& t1,int br) {
	max = br;
	polozaj = t1;
}
int oruzje::getstatus() {
	return stanje;
}
//zad 3
void meta::postavi(tocka& t1, tocka& t2) {
	tocka1 = t1;
	tocka2 = t2;
}
void meta::get_position(double& dg, double& gg) {
	double niz[3];
	tocka1.dohvati(niz);
	double niz1[3];
	tocka2.dohvati(niz1);
	dg = niz[2];
	gg = niz[2];
}
void meta::pogodena(bool b) {
	shot = b;
}

int generate() {
	int n=0;
	cout << "Unesite broj meta" << endl;
	cin >> n;
	meta* niz = new meta[n];
	for (int i = 0; i < n; i++) {
		tocka temp1;
		tocka temp2;
		temp1.postavi_sluc(10, 15);
		temp2.postavi_sluc(10, 15);
		niz[i].postavi(temp1, temp2);
	}
	oruzje o;
	tocka temp1;
	temp1.postavi_sluc(10, 15);
	int br_met = rand() % 15 + 3;
	o.set(temp1, br_met);
	int count = pucaj(niz, o, n);
	delete[] niz;
	return count;
}
//zad 4
int pucaj(meta* mete, oruzje& oruzje, int n) {
	int counter = 0;
	double z = oruzje.z_koordinata();
	for (int i = 0; i < n; i++) {
		if (!oruzje.getstatus())
			oruzje.reload();
		double dg;
		double gg;
		mete[i].get_position(dg, gg);
		if (z >= dg && z <= gg) {
			counter++;
			mete[i].pogodena(true);
		}
		else {
			mete[i].pogodena(false);
		}
		oruzje.shoot();
	}
	return counter;
}
