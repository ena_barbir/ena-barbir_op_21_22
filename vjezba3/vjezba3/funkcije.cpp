#include "header.hpp"
//zad1
void create(vector<int>& v, int n) {
	vector<int>t(n, 0);
	v = t;
}
void unos(vector<int>& v, int dg, int gg) {
	int x = dg;
	while (x >= dg && x <= gg) {
		cout << "Unesite element: " << endl;
		cin >> x;
		v.push_back(x);
	}
}
//zad2
vector<int> fun(vector<int>& v1, vector<int>& v2) {
	vector<int> v3;
	for (int i = 0; i < v1.size(); i++) {
		if (!(count(v2.begin(), v2.end(), v1[i]))) {
			v3.push_back(v1[i]);
		}
	}
	return v3;
}

//zad3

bool cmpfnc(int a, int b) {
	return a < b;
}

void sort_and_insert(vector<int>& v) {
	sort(v.begin(), v.end(), cmpfnc);
	v.insert(v.begin(),0);
	int sum = 0;
	for (int i = 0; i < v.size(); i++) {
		sum += v[i];
	}
	v.insert(v.end(), sum);

}
void print(const vector<int>& v) {
	for (int i = 0; i < v.size(); i++) {
		cout << v[i] << " ";
	}
	cout << endl;
}

void delete_element(int x,vector<int>&v) {
	int ind = 0;
	if (!count(v.begin(), v.end(), x)) {
		return;
	}
	for (int i = 0; i < v.size(); i++) {
		if (v[i] == x) {
			ind = i;
			break;
		}
	}
	v.erase(v.begin()+ind);
}
//zad5
int count_substrings(string s, string substring) {
	int size = substring.size();
	int counter = 0;
	for (int i = 0; i < s.size(); i++) {
		if (s[i] == substring[0]) {
			string temp = s.substr(i,size);
			if (temp == substring) {
				counter++;
				i += size-1;
			}
		}
	}
	return counter;
}
void input_string(vector<string>& s,int x) {
	string temp;
	int i = 0;
	while (i<x) {
		cout << "Unesite string" << endl;
		cin >> temp;
		s.push_back(temp);
		i++;
	}
}
//zad6
void reverse_and_sort(vector<string>&s) {
	int x;
	cout << "Unesite broj stringova koje zelite" << endl;
	cin >> x;
	input_string(s,x);
	for (int i = 0; i < s.size(); i++) {
		reverse(s[i].begin(), s[i].end());
	}
	sort(s.begin(), s.end());
}
void print(const vector<string>& s) {
	for (int i = 0; i < s.size(); i++) {
		cout << s[i] << endl;
	}
}
//pomocne funkcije
void input_elements(vector<int>& v) {
	int x;
	cout << "Unesite broj elemenata: " << endl;
	cin >> x;
	for (int i = 0; i < x; i++) {
		int temp;
		cout << "Unesite element" << i + 1 << endl;
		cin >> temp;
		v.push_back(temp);
	}
}
void input_elements(vector<string>&v) {
	int x;
	cout << "Unesite broj elemenata: " << endl;
	cin >> x;
	for (int i = 0; i < x; i++) {
		string temp;
		cout << "Unesite element" << i + 1 << endl;
		cin >> temp;
		v.push_back(temp);
	}
}
