#pragma once

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>

using namespace std;
void sort_and_insert(vector<int>& v);
bool cmpfnc(int a, int b);
vector<int> fun(vector<int>& v1, vector<int>& v2);
void unos(vector<int>& v, int dg, int gg);
void print(const vector<int>& v);
void delete_element(int x, vector<int>& v);
int count_substrings(string s, string substring);
void reverse_and_sort(vector<string>& s);
void input_string(vector<string>& s,int x);
void print(const vector<string>& s);
void input_elements(vector<int>& v);
void input_elements(vector<string>& v);