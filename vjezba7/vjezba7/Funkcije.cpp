#include "Header.hpp"

Board::Board() {
	this->height = 12;
	this->lenght = 12;
	this->outline = '#';
	table = new char* [height];
	for (int i = 0; i < height; i++) {
		table[i] = new char[12];
	}
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < lenght; j++)
		{
			if (i == 0 || i == height - 1 || j == 0 || j == lenght - 1) {
				table[i][j] = outline;
			}
			else {
				table[i][j] = ' ';
			}
		}
	}
}
Board::Board(int h, int l, char symbol) {
	this->height = h+2;
	this->lenght = l+2;
	this->outline = symbol;
	table = new char* [height];
	for (int i = 0; i < height; i++) {
		table[i] = new char[lenght];
	}
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < lenght; j++)
		{
			if (i == 0 || i == height - 1 || j == 0 || j == lenght - 1) {
				table[i][j] = outline;
			}
			else {
				table[i][j] = ' ';
			}
		}
	}
}
Board::Board(const Board& b) {
	this->height = b.height;
	this->lenght = b.lenght;
	this->outline = b.outline;
	table = new char* [height];
	for (int i = 0; i < height; i++) {
		table[i] = new char[lenght];
	}
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < lenght; j++)
		{
			if (i == 0 || i == height - 1 || j == 0 || j == lenght - 1) {
				table[i][j] = outline;
			}
			else {
				table[i][j] = ' ';
			}
		}
	}
	
}
Board::~Board() {
	for (int i = 0; i < height; i++) {
		delete[]table[i];
	}
	delete table;
}
void Board::Display() {
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < lenght; j++) {
			cout << table[i][j];
		}
		cout << endl;
	}
}
bool Board::check_coordinate(Point& p) {
	if (p.x <= 0 || p.y <= 0 || p.x >= height || p.y >= lenght) {
		cout << "Point out of coordinates" << endl;
		return false;
	}
	else
		return true;
}
void Board::draw_char(Point& p, char ch) {
	Point pt;
	pt.x = round(p.x);
	pt.y = round(p.y);
	if (!check_coordinate(pt)) {
		return;
	}
	pt.x = height - pt.x;
	table[(int)pt.x][(int)pt.y] = ch;
}
void Board::clear() {
	for (int i = 1; i < height - 1; i++) {
		for (int j = 1; j < lenght - 1; j++) {
			table[i][j] = ' ';
		}
	}
}
void Board::draw_up_line(const Point& p, char ch) {
	Point pt;
	pt.x = round(p.x);
	pt.y = round(p.y);
	if (!check_coordinate(pt)) {
		return;
	}
	int h = height - pt.x;
	while (h) {
		table[h][(int)pt.y] = ch;
		h--;
	}
}
double Point::distance(const Point& p) {
	return sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
}
void Point::roundall() {
	if (x - (int)x > 0.49) {
		x++;
	}
	if (y - (int)y > 0.49) {
		y++;
	}
	x = (double)((int)x);
	y = (double)((int)y);

}
void Board::draw_line(Point p1, Point p2,char ch) {
	
	if(!check_coordinate(p1) || !check_coordinate(p2)){
		cout << "Invalid dimension" << endl;
		return;
	}
	//Okretanje slike zato sto je 0 0 gore lijevo zbog alokacije
	p1.x = height - p1.x; 
	p2.x = height - p2.x;
	//Izracun polovista
	double x = (p1.x + p2.x) / 2;
	double y =(p1.y + p2.y) / 2;
	Point half(x, y);
	//Zbog pikselizacije ekrana prekidamo funkciju kad udaljenost izmedu 2 tocke
	//postane manja(ili jednaka) 1
	if (p1.distance(p2) < 1) {
		table[(int)(height - half.y)][(int)(half.x)] = ch;
		return;
	}
	//Rekurzivni poziv lijevo
	draw_line(p1, half, ch);
	//Rekurzivni poziv desno
	draw_line(half, p2, ch);
}
