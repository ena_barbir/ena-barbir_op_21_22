#include<iostream>
#include <cstdlib>
using namespace std;
struct Point {
	double x;
	double y;
	Point(int a, int b) {
		x = a;
		y = b;
	}
	Point(double a, double b) {
		x = a;
		y = b;
	}
	Point() {
		x = 0;
		y = 0;
	}
	double distance(const Point& p);
	void roundall();
};
class Board {
private:
	char** table;
	int lenght;
	int height;
	char outline;
	bool check_coordinate(Point& p);
public:
	Board();
	Board(int x, int y, char c);
	Board(const Board &b1);
	~Board();
	void Display();
	void draw_char(Point& p, char ch);
	void clear();
	void draw_up_line(const Point& p, char ch);
	void draw_line(Point p, Point p1,char ch);

};
